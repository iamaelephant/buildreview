exports.shorthands = undefined;

exports.up = (pgm) => {
    pgm.dropColumns('users', ['googleId']);
    pgm.addColumn('users', {
        'googleid': { 
            type: 'varchar(1000)',
            notNull: false,
        }
    });
};

exports.down = (pgm) => {
    pgm.dropColumns('users', ['googleid']);
    pgm.addColumn('users', {
        'googleId': { 
            type: 'varchar(1000)',
            notNull: false,
        }
    });
};
