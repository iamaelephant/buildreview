exports.shorthands = undefined;

exports.up = (pgm) => {
    pgm.createTable('users', {
        id: 'id',
        email: {
            type: 'varchar(1000)', notNull: true
        },
        authType: {
            type: 'varchar(1000)', notNull: true
        },
        authToken: {
            type: 'varchar(1000)', notNull: true
        },
        displayName: {
            type: 'varchar(1000)', notNull: false
        }
    });

    pgm.addColumn('reviews', {
        'userId': { 
            type: 'integer',
            notNull: true,
            references: 'users',
            onDelete: 'cascade'
        }
    });
};

exports.down = (pgm) => {
    pgm.dropColumns('reviews', ['userId']);
    pgm.deleteTable('users');
};
