exports.shorthands = undefined;

exports.up = (pgm) => {
    pgm.alterColumn('users', 'authToken', { notNull: false });
};

exports.down = (pgm) => {
    pgm.alterColumn('users', 'authToken', { notNull: true });
};
