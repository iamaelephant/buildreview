exports.shorthands = undefined;

exports.up = (pgm) => {
    pgm.addColumn('users', {
        'googleId': { 
            type: 'varchar(1000)',
            notNull: false,
        }
    });
};

exports.down = (pgm) => {
    pgm.dropColumns('users', ['googleId']);
};
