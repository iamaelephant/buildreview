exports.shorthands = undefined;

exports.up = (pgm) => {
    pgm.createTable("builders", {
        id: "id",
        name: { type: "varchar(1000)", notNull: true },
        createdAt: {
            type: "timestamp",
            notNull: true,
            default: pgm.func("current_timestamp")
        }
    });
    pgm.createTable("reviews", {
        id: "id",
        builderId: {
            type: "integer",
            notNull: true,
            references: "builders",
            onDelete: "cascade"
        },
        body: { type: "text", notNull: true },
        createdAt: {
            type: "timestamp",
            notNull: true,
            default: pgm.func("current_timestamp")
        }
    });
    pgm.createIndex("reviews", "builderId");
};

exports.down = (pgm) => {
    pgm.deleteTable("reviews");
    pgm.deleteTable("builders");
    pgm.deleteIndex("reviews", "builderId");
};
