const React = require('react');
const Layout = require('./layouts/main-layout');

class MainPage extends React.Component {

    renderLoggedIn() {
        return (
            <div>
                <span>Welcome back, </span><span>{this.props.displayName}</span>
                <div>
                    <a href="/reviews/create">Create</a>
                </div>
            </div>
        );
    }

    renderLoggedOut() {
        return (
            <div>
                <div>
                    <a href="/login/google">Login with Google</a>
                </div>
            </div>
        );
    }

    render() {
        const hasUser = !!this.props.displayName;
        const content = hasUser ? this.renderLoggedIn() : this.renderLoggedOut();
        return (<Layout>{content}</Layout>);
    }
}

module.exports = MainPage;