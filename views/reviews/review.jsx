const React = require('react');
const Layout = require('../layouts/main-layout');

module.exports = (props) => {
    return (
        <Layout>
            <h2>Review by {props.reviewer.displayName}</h2>

            <div>

                <span>{props.review.body}</span>

            </div>
        </Layout>
    );
}