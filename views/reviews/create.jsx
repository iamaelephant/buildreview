const Layout = require('../layouts/main-layout');
const React = require('react');

class Create extends React.Component {
    render() {
        return (
            <Layout>
                <div>
                    <h2>CREATE!</h2>
            
                    <form action='./create' method='post'>
                        <div>
                            <label for='builder'>Builder:</label>
                            <select id='builder' name='builderId'>
                                <option value='1' label='FIRST'></option>
                            </select>
            
                            <label for='review-body'>Review body</label>
                            <textarea id='review-body' name='body'></textarea>
            
                            <button type='submit'>Create review</button>
                        </div>
                    </form>
                </div>
            </Layout>
        );
    }
}

module.exports = Create;