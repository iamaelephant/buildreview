const React = require('react');
const Layout = require('../layouts/main-layout');

module.exports = (props) => {
    return (
        <Layout>
            <h2>Review</h2>
                
            <div>
                <ul>
                    {props.reviews.map(r => {
                        return (<li><a href={`./${r.id}`}>{JSON.stringify(r.createdAt)}</a></li>);
                    })}
                </ul>
            </div>
        </Layout>
            
    );
}