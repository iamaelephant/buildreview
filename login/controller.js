const { findOrCreateGoogleUser } = require('../data/users');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

exports.loginGet = async (req, res) => {
    res.render(__dirname + '/views/login')
};

exports.googleGet = passport.authenticate('google', { scope: ['profile'] });

exports.googleCallbackGet = passport.authenticate(
    'google', 
    { failureRedirect: '/login' }
);

exports.loginSuccess = (req, res) => {
    res.redirect('/');
}

const GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
const GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET;

passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: `/login/google-callback`,
    proxy: true
},
    async function (accessToken, refreshToken, profile, cb) {
        const user = await findOrCreateGoogleUser(profile);
        return cb(null, user);
    }
));
