const loginController = require('./controller');

const express = require('express');
const router = express.Router();  

router.get('/', loginController.loginGet);
router.get('/google', loginController.googleGet);
router.get('/google-callback', 
    loginController.googleCallbackGet, 
    loginController.loginSuccess
);

module.exports = router;