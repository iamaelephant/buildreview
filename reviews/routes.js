const reviewController = require('./controller');

const express = require('express');
const router = express.Router();  

router.get('/create', reviewController.createGet);
router.post('/create', reviewController.createPost);

router.get('/', reviewController.listGet);
router.get('/:reviewId', reviewController.reviewGet);

module.exports = router;