const { createReview, getReview, getReviews } = require('./reviews.js');
const { getUser } = require('../data/users.js');

exports.createGet = async (req, res) => {
    if (req.user) {
        res.render('reviews/create')
    }
    else {
        res.redirect('/login');
    }
};

exports.createPost = async (req,res) => {
    const reviewId = await createReview(req.body, req.user);
    res.redirect(`./${reviewId}`);
};

exports.reviewGet = async (req, res) => {
    const { reviewId } = req.params;
    const review = await getReview(reviewId);
    const reviewer = await getUser(review.userId);
    res.render('reviews/review', {reviewer, review})
};

exports.listGet = async (req, res) => {
    const reviews = await getReviews();
    res.render('reviews/list', { reviews });
}