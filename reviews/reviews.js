const { Pool } = require('pg');

const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
});

const createReview =  async (review, user) => {
    const client = await pool.connect()
    
    const result = await client.query(`
        INSERT INTO reviews
            ("id", "builderId", "body", "userId")
        VALUES
            (DEFAULT, $1, $2, $3)
        RETURNING id`,
        [review.builderId, review.body, user.id]);

    return result.rows[0].id;
}

const getReview = async (reviewId) => {
    const client = await pool.connect()

    const result = await client.query(`
        SELECT * FROM reviews WHERE "id" = $1`,
        [ reviewId ]
    );

    return result.rows[0];
}

const getReviews = async () => {
    const client = await pool.connect()

    const result = await client.query(`
        SELECT * FROM reviews`
    );

    return result.rows;
}

module.exports = {
    createReview,
    getReview,
    getReviews,
}