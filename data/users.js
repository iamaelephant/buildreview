const { Pool } = require('pg');

const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
});

const findUser = async (googleId, client) => {
    const result = await client.query('SELECT * FROM users WHERE googleid = $1', [googleId]);

    return result.rows && result.rows[0];
}

const getUser = async (userId) => {
    const client = await pool.connect();
    const result = await client.query('SELECT * FROM users WHERE id = $1', [userId]);

    return result.rows && result.rows[0];
}

const findOrCreateGoogleUser =  async (profile) => {
    const client = await pool.connect();
    const user = await findUser(profile.id, client);
    
    if (!user) {
        client.query(`
        INSERT INTO users 
            ("email", "authType", "displayName", "googleid")
        VALUES 
            ($1, $2, $3, $4)`,
        [profile.email, 'google', profile.displayName, profile.id]);
    }
    
    return await findUser(profile.id, client);
}

module.exports = {
    findOrCreateGoogleUser,
    getUser
}