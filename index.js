const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000
const passport = require('passport');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const expressReactViews = require('express-react-views')

const app = express();

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// set up passport login and session
app.use(session({ 
  secret: process.env.SESSION_SECRET || 'big-bad-secret',
  saveUninitialized: false,
  resave: false,
}));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

// view engine
app.use(express.static(path.join(__dirname, 'public')))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jsx')
app.engine('jsx', expressReactViews.createEngine());

// set up routes
const reviewsRouter = require('./reviews/routes');
const loginRouter = require('./login/routes');
app.use('/reviews', reviewsRouter);
app.use('/login', loginRouter);

app.get('/', (req, res) => res.render('index', {...req.user}));

// hit it
app.listen(PORT, () => console.log(`Listening on ${ PORT }`));